package com.disney.projects;

import java.util.Scanner;

public class GeneralFormula {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("a = ? ");
        int a = in.nextInt();

        System.out.println("b = ? ");
        int b = in.nextInt();

        System.out.println("c = ? ");
        int c = in.nextInt();

        if (a > 0) {
            
            double firstPart = Math.pow(b, 2) - (4 * a * c);

            if (firstPart < 0) {

                System.out.println("Root negative");
                
            } else {
                
                double x1 = (-b + Math.sqrt(firstPart)) / (2 * a);
                double x2 = (-b - Math.sqrt(firstPart)) / (2 * a);
                System.out.println("X1 = " + x1 + "\nX2 = " + x2);
                
            }
        } else {
            System.out.println("indeterminate limit");
        }
    }

}
