package com.disney.projects.abstracts;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InvokerSoccerPlayers {

    public static final int ONE = 1;
    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        Map<Integer, SoccerPlayer> map = new HashMap<Integer, SoccerPlayer>();

        map.put(indexPlusOne(map), new Goalkeeper("Enrique", "Arteaga", 20));
        map.put(indexPlusOne(map), new Defense("Francisco", "Díaz", 24));
        map.put(indexPlusOne(map), new Midfielder("Miguel", "Ledezma", 26));
        map.put(indexPlusOne(map), new Forward("Luis", "Castillo", 20));

        String retry = null;

        showMap(map);

        do {
            System.out.println("\nWhat do you want to do \n1)Add Player \n2)Remove Player \n3)View Detail");
            int option = SCANNER.nextInt();

            switch (option) {
            case 1:
                map = add(map);
                showMap(map);
                break;
            case 2:
                map = remove(map);
                showMap(map);
                break;
            case 3:
                viewDetail(map);
                break;

            default:
                System.out.println("Invalid option");
                break;
            }

            System.out.println("Do you want to do another operation? (Y/N) :");
            retry = SCANNER.next();

        } while (retry.toLowerCase().equals("y"));

        System.out.println("Bye");

    }

    public static void showMap(Map<Integer, SoccerPlayer> map) {

        Map<Integer, SoccerPlayer> mapTmpShow = map;

        for (Map.Entry<Integer, SoccerPlayer> player : mapTmpShow.entrySet()) {

            System.out.println(player.getKey() + ") Name :" + player.getValue().getName());

        }

    }

    public static Map<Integer, SoccerPlayer> add(Map<Integer, SoccerPlayer> map) {

        Map<Integer, SoccerPlayer> mapTmpAdd = map;
        String name = null;
        String lastName = null;
        int age = 0;

        System.out.println("\nWhich player do you want to add \n1)Goalkeeper\n2)Defense\n3)Midfielder\n4)Forward");
        int position = SCANNER.nextInt();

        if (position <= 4) {
            System.out.print("Name : ");
            name = SCANNER.next();
            System.out.print("Last Name : ");
            lastName = SCANNER.next();
            System.out.print("Age : ");
            age = SCANNER.nextInt();
        }

        switch (position) {
        case 1:
            mapTmpAdd.put(indexPlusOne(mapTmpAdd), new Goalkeeper(name, lastName, age));
            break;
        case 2:
            mapTmpAdd.put(indexPlusOne(mapTmpAdd), new Defense(name, lastName, age));
            break;
        case 3:
            mapTmpAdd.put(indexPlusOne(mapTmpAdd), new Midfielder(name, lastName, age));
            break;
        case 4:
            mapTmpAdd.put(indexPlusOne(mapTmpAdd), new Forward(name, lastName, age));
            break;

        default:
            System.out.println("Invalid option");
            break;
        }

        return mapTmpAdd;

    }

	private static int indexPlusOne(Map<Integer, SoccerPlayer> mapTmpAdd) {
		return mapTmpAdd.size() + ONE;
	}

    public static Map<Integer, SoccerPlayer> remove(Map<Integer, SoccerPlayer> map) {

        Map<Integer, SoccerPlayer> mapTmpRemove = map;

        showMap(mapTmpRemove);

        System.out.println("What player do you want to eliminate?");
        int remove = SCANNER.nextInt();

        for (Map.Entry<Integer, SoccerPlayer> player : mapTmpRemove.entrySet()) {

            if (remove <= player.getKey()) {
                mapTmpRemove.remove(remove);
            }
        }

        return mapTmpRemove;

    }

    public static void viewDetail(Map<Integer, SoccerPlayer> map) {

        Map<Integer, SoccerPlayer> mapTmpView = map;

        showMap(mapTmpView);

        System.out.println("what player do you want to see detail?");
        int view = SCANNER.nextInt();
        
        
        for (Map.Entry<Integer, SoccerPlayer> player : mapTmpView.entrySet()) {
            if (view <= player.getKey()) {
                player.getValue().information();
            }
        }
    }
}
