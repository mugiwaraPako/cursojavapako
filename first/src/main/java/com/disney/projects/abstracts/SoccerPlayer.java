package com.disney.projects.abstracts;

public abstract class SoccerPlayer {

    String name;
    String lastName;
    int age;

    SoccerPlayer() {

    }

    SoccerPlayer(String name, String lastName, int age) {

        this.name = name;
        this.lastName = lastName;
        this.age = age;

    }
    

    void information() {

        System.out.println("Name : " + getName() + "\nLastName : " + getLastName() + "\nAge : " + getAge());

        train();

    }

    abstract void train();

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    int getAge() {
        return age;
    }

    void setAge(int age) {
        this.age = age;
    }

}
