package com.disney.projects.abstracts;

public class Goalkeeper extends SoccerPlayer {

    Goalkeeper(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    void train() {
        System.out.println("Training for Goalkeeper\n");
    }

}
