package com.disney.projects;

import java.util.Scanner;

public class ScannerExample {
    
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Your name ");
        String nombre = in.next();
        
        System.out.println("Hola " + nombre);
        
    }

}
