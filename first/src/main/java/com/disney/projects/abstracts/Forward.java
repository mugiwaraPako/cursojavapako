package com.disney.projects.abstracts;

public class Forward extends SoccerPlayer {

    Forward(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    void train() {
        System.out.println("Training for Goalkeeper\n");
    }

}
