package com.disney.projects.abstracts;

import java.util.Scanner;

public class Atm {

    static final int ZERO = 0;
    static final int MAXAMOUNT = 10000;

    /**
     * @param args
     */
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String retry = null;
        StringBuffer output = new StringBuffer();
        int[] tickets = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };

        do {

            System.out.println("How much money do you withdraw?");
            int rode = in.nextInt();

            if (rode <= MAXAMOUNT) {

                for (int i : tickets) {

                    int response = rode / i;

                    if (response > ZERO) {

                        output.append(response + " ticket of $" + i + "\n");

                        rode = rode % i;

                    }

                }

                System.out.println(output);

            } else {
                System.out.println("You can not withdraw more than $10,000");
            }

            System.out.println("Do you want to withdraw more money? (Y/N) :");
            retry = in.next();

        } while (retry.toLowerCase().equals("y"));

        System.out.println("Bye");
    }

}
