package com.disney.projects.abstracts;

public class Midfielder extends SoccerPlayer {

    Midfielder(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    void train() {
        System.out.println("Training for Midfielder\n");
    }

}
