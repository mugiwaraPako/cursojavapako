package com.disney.projects.abstracts;

public class Defense extends SoccerPlayer{

    
    Defense(String name, String lastName, int age){
        super(name, lastName, age);
    }
    
    
    @Override
    void train() {
        System.out.println("Training for Defenses\n");
    }

}
